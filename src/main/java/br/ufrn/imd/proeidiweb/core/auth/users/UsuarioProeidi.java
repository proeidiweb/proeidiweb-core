package br.ufrn.imd.proeidiweb.core.auth.users;

import br.ufrn.imd.proeidiweb.usuario.dto.PapelDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UsuarioProeidi {
    private Long id;

    private String nome;
    private String login;
    private String email;

    private PapelDTO papelSelecionado;
    private List<PapelDTO> papeis;
}
