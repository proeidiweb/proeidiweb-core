package br.ufrn.imd.proeidiweb.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(scanBasePackages = "br.ufrn.imd.proeidiweb", exclude = { FlywayAutoConfiguration.class })
@EnableJpaRepositories(basePackages = "br.ufrn.imd.proeidiweb")
@EntityScan("br.ufrn.imd.proeidiweb")
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableSpringDataWebSupport
@EnableJpaAuditing
@EnableAsync
public class ProEidiWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProEidiWebAppApplication.class, args);
	}

}
