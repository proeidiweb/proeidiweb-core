package br.ufrn.imd.proeidiweb.core.auth;

import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.auth.validators.JwtValidatorUtils;
import br.ufrn.imd.proeidiweb.core.auth.validators.SenhaValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.domain.enums.TipoPapel;
import br.ufrn.imd.proeidiweb.usuario.dto.PapelDTO;
import br.ufrn.imd.proeidiweb.usuario.repository.PapelRepository;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AutenticacaoService {
    private final PessoaRepository pessoaRepository;
    private final PapelRepository papelRepository;

    private ProeidiAutenticacaoResponse createResponse(Pessoa pessoa) {
        ProeidiAutenticacaoResponse response = new ProeidiAutenticacaoResponse();

        List<Papel> papeis = this.papelRepository.findAllByPessoa(pessoa.getId()).orElseThrow(ResourceNotFoundException::new);
        List<PapelDTO> papeisDTO = new ArrayList<>();
        for (Papel papel : papeis) {
            papeisDTO.add(new PapelDTO(papel));
        }

        UsuarioProeidi usuario = new UsuarioProeidi();
        usuario.setId(pessoa.getId());
        usuario.setNome(pessoa.getNome());
        usuario.setLogin(pessoa.getUsuario());
        usuario.setEmail(pessoa.getEmail());

        usuario.setPapeis(papeisDTO);
        response.setPapeis(papeisDTO);

        if (papeisDTO.size() == 1) {
            response.setPapelSelecionado(papeisDTO.get(0));
            usuario.setPapelSelecionado(papeisDTO.get(0));
        }

        response.setUsuario(usuario);

        Gson gson = new Gson();
        String json = gson.toJson(usuario);

        response.setToken(JwtValidatorUtils.createJWT(usuario.getId().toString(), "JWT Issuer", json, 800000000));

        return response;
    }

    public ProeidiAutenticacaoResponse autenticar(String login, String senha) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaRepository.findByUsuarioOrEmailOrCpfAndAtivo(login).orElseThrow(ResourceNotFoundException::new);

        if(!SenhaValidatorUtils.encryptPassword(senha).equals(pessoa.getSenha())) {
            throw new BusinessRuleException("Login ou Senha incorretos");
        }

        this.startPapeis(pessoa);

        return createResponse(pessoa);
    }

    public ProeidiAutenticacaoResponse selecionarPapel(UsuarioProeidi usuario, Long idPapel) {
        Pessoa pessoa = this.pessoaRepository.findById(usuario.getId()).orElseThrow(ResourceNotFoundException::new);
        Papel papel = this.papelRepository.findById(idPapel).orElseThrow(ResourceNotFoundException::new);

        PapelDTO papelDTO = new PapelDTO(papel);
        usuario.setPapelSelecionado(papelDTO);

        ProeidiAutenticacaoResponse response = this.createResponse(pessoa);

        response.setUsuario(usuario);
        response.setPapelSelecionado(papelDTO);

        Gson gson = new Gson();
        String json = gson.toJson(usuario);

        response.setToken(JwtValidatorUtils.createJWT(usuario.getId().toString(), "JWT Issuer", json, 8000000));

        return response;
    }

    private void startPapeis(Pessoa pessoa) {
        if (this.papelRepository.findAll().size() == 0) {
            Papel coordenador = new Papel();
            coordenador.setNome(TipoPapel.COORDENADOR);
            coordenador.setPessoaCadastro(pessoa);
            coordenador.setPessoaUltimaAtualizacao(pessoa);
            coordenador.setAtivo(true);
            this.papelRepository.save(coordenador);

            Papel monitor = new Papel();
            monitor.setNome(TipoPapel.MONITOR);
            monitor.setPessoaUltimaAtualizacao(pessoa);
            monitor.setPessoaCadastro(pessoa);
            monitor.setAtivo(true);
            this.papelRepository.save(monitor);

            Papel aluno = new Papel();
            aluno.setNome(TipoPapel.ALUNO);
            aluno.setPessoaUltimaAtualizacao(pessoa);
            aluno.setPessoaCadastro(pessoa);
            aluno.setAtivo(true);
            this.papelRepository.save(aluno);

            List<Papel> papeis = Arrays.asList(coordenador, monitor);
            pessoa.setPapeis(papeis);
            this.pessoaRepository.save(pessoa);
        }
    }
}
