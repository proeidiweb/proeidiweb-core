package br.ufrn.imd.proeidiweb.core.exceptions;

public class AuthorizationException extends Exception {
    public AuthorizationException() { super(); }
    public AuthorizationException(String message) { super(message); }
}
