package br.ufrn.imd.proeidiweb.core.auth.resolvers;

import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.auth.validators.JwtValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.AuthorizationException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class ProeidiUserRequestResolver {

    private String getJwtToken(HttpServletRequest request) throws AuthorizationException {
        String authHeader = request.getHeader("Authorization");
        if (authHeader == null) {
            throw new AuthorizationException("Missing Authorization Request Header");
        } else {
            return authHeader.replace("Bearer ", "");
        }
    }

    public UsuarioProeidi resolve(HttpServletRequest request) throws AuthorizationException {
        String jwtToken = getJwtToken(request);
        if (jwtToken == null) {
            throw new AuthorizationException("Sem token JWT informado");
        }

        UsuarioProeidi user = JwtValidatorUtils.decodeJWT(jwtToken);

        request.setAttribute(ProeidiSessionAttributes.USUARIO, user);
        request.setAttribute("authToken", jwtToken);

        return user;
    }

}
