package br.ufrn.imd.proeidiweb.core.auth;

import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.usuario.dto.PapelDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class AutenticacaoRestController {
    private final AutenticacaoService autenticacaoService;

    @PostMapping(path = "/login")
    public ProeidiAutenticacaoResponse autenticar(@RequestParam("login") String login, @RequestParam("password") String senha, HttpServletResponse response) {
        try {
            return this.autenticacaoService.autenticar(login, senha);
        } catch (ResourceNotFoundException | BusinessRuleException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PostMapping(path = "/selecionar-papel")
    public ProeidiAutenticacaoResponse selecionarPapel(UsuarioProeidi usuario, @RequestParam("id_papel") Long idPapel) {
        try {
            return this.autenticacaoService.selecionarPapel(usuario, idPapel);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GetMapping(path = "/me")
    public ProeidiAutenticacaoResponse me(UsuarioProeidi usuario) {
        return this.selecionarPapel(usuario, usuario.getPapelSelecionado().getId());
    }

    @GetMapping(path = "/papeis")
    public List<PapelDTO> papeis(UsuarioProeidi usuario) {
        return usuario.getPapeis();
    }
}
