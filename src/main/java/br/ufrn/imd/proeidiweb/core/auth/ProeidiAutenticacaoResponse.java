package br.ufrn.imd.proeidiweb.core.auth;

import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.usuario.dto.PapelDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProeidiAutenticacaoResponse {
    private UsuarioProeidi usuario;

    private PapelDTO papelSelecionado;
    private List<PapelDTO> papeis;

    private String errorMessage;
    private String token;
}
