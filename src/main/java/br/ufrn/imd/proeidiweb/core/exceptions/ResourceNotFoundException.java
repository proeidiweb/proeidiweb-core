package br.ufrn.imd.proeidiweb.core.exceptions;

public class ResourceNotFoundException extends ProEIDIWebException {

    public ResourceNotFoundException() {}
    public ResourceNotFoundException(String msg) { super(msg); }
    public ResourceNotFoundException(Exception e) { super(e); }
}
