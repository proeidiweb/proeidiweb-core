package br.ufrn.imd.proeidiweb.core.auth.resolvers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class ProeidiWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired
    ProeidiUserRequestResolver userResolver;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new ProeidiUserArgumentResolver(userResolver));
    }
}
