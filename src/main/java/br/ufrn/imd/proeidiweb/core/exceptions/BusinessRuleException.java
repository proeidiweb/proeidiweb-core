package br.ufrn.imd.proeidiweb.core.exceptions;

public class BusinessRuleException extends Exception{
    public BusinessRuleException() {
        super();
    }
    public BusinessRuleException(String message) {
        super(message);
    }
}
