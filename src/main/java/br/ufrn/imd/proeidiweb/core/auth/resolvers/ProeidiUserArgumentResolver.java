package br.ufrn.imd.proeidiweb.core.auth.resolvers;

import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

public class ProeidiUserArgumentResolver implements HandlerMethodArgumentResolver {

    private ProeidiUserRequestResolver userResolver;

    public ProeidiUserArgumentResolver(ProeidiUserRequestResolver userResolver) { this.userResolver = userResolver; }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(UsuarioProeidi.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        return userResolver.resolve(request);
    }
}
