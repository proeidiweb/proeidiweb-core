package br.ufrn.imd.proeidiweb.core.exceptions;

public class ProEIDIWebException extends RuntimeException {
    public ProEIDIWebException(){}

    public ProEIDIWebException(String msg) { super(msg); }
    public ProEIDIWebException(Exception e) { super(e); }
    public ProEIDIWebException(String msg, Exception e) { super(msg, e); }

    public boolean isNotificavel() { return true; }
}
