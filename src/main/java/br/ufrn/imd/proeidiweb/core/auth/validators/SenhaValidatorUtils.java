package br.ufrn.imd.proeidiweb.core.auth.validators;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class SenhaValidatorUtils {
    private static final String salt = "PR03IDi@3bc()R3";
    public static String encryptPassword(String senha) {
        return Hashing.sha512().hashString(senha.concat(salt), StandardCharsets.UTF_8).toString();
    }
}
