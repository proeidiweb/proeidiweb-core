package br.ufrn.imd.proeidiweb.comum.query;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Classe para filtrar os dados do banco paginado
 * @Autor Aroldo Felix
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QueryFilter {
    private String search;
    private int page = 1;
    private int pageSize = 20;
}
