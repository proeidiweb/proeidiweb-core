package br.ufrn.imd.proeidiweb.comum.service;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;

import java.util.List;

public interface ServiceComumComplete<T, S extends TipoDTO> extends ServiceComum<T> {
    List<S> findAllByAtivo();
}
