package br.ufrn.imd.proeidiweb.comum.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Classe para voltar uma resposta paginada
 * @param <T> Tipo do conteúdo
 */
@Getter
@Setter
@NoArgsConstructor
public class PageResponse<T> {
    private List<T> content;
    private int currentPage;
    private int pageSize;
    private Long totalElements;
    private Long totalPages;
}
