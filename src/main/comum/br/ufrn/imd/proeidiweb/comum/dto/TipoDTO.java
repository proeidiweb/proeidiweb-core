package br.ufrn.imd.proeidiweb.comum.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class TipoDTO {
    protected Long id;
    protected String nome;
}
