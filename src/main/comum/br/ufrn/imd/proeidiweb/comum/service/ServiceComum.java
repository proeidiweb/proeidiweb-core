package br.ufrn.imd.proeidiweb.comum.service;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;

public interface ServiceComum<T> {
    Long save(UsuarioProeidi user, T objeto) throws BusinessRuleException;
    T findOne(Long id);
    PageResponse<T> findAllPageable(QueryFilter filtro);
    void desativar(UsuarioProeidi user, Long id) throws BusinessRuleException;
    void validar(T objeto) throws BusinessRuleException;
}
