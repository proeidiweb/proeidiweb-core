package br.ufrn.imd.proeidiweb.test;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/teste")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class TestRestController {
    @GetMapping(path = "/")
    public String helloWorld() {
        return "Hello World";
    }
}
