package br.ufrn.imd.proeidiweb.usuario.repository;

import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.domain.enums.TipoPapel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PapelRepository extends JpaRepository<Papel, Long> {
    @Query("SELECT p.papeis FROM Pessoa p WHERE p.id = :pessoa AND p.ativo = TRUE")
    Optional<List<Papel>> findAllByPessoa(@Param("pessoa") Long idPessoa);

    @Query("SELECT p FROM Papel p WHERE p.nome = :nome")
    Papel findPapelByNome(@Param("nome") TipoPapel nome);
}
