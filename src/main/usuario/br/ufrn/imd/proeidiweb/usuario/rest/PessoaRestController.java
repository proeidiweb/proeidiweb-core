package br.ufrn.imd.proeidiweb.usuario.rest;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.comum.validators.PermissaoValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.dto.PessoaListItemDTO;
import br.ufrn.imd.proeidiweb.usuario.service.PessoaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/pessoa")
@RequiredArgsConstructor
public class PessoaRestController {
    private final PessoaService pessoaService;

    @GetMapping
    public PageResponse<PessoaListItemDTO> findAll(UsuarioProeidi user, QueryFilter filtro, @RequestParam(name = "monitores", required = false, defaultValue = "true") boolean somenteMonitores) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        return this.pessoaService.findAllByAtivo(filtro, somenteMonitores);
    }

    @GetMapping(path = "/{id}")
    public Pessoa findOne(UsuarioProeidi user, @PathVariable(name = "id") Long idPessoa) {
        return this.pessoaService.findOne(idPessoa);
    }

    @PostMapping(path = "/monitor")
    public Long saveMonitor(UsuarioProeidi user, @RequestBody Optional<Pessoa> pessoa) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        if (pessoa.isEmpty()) {
            throw new BusinessRuleException("Corpo vazio");
        }
        return this.pessoaService.saveMonitor(user, pessoa.get());
    }

    @PostMapping(path = "/aluno")
    public Long saveAluno(UsuarioProeidi user, @RequestBody Optional<Pessoa> pessoa) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        if (pessoa.isEmpty()) {
            throw new BusinessRuleException("Corpo vazio");
        }
        // TODO fazer fluxo para aluno
        return null;
    }

    @DeleteMapping(path = "/{id}")
    public void deletePessoa(UsuarioProeidi user, @PathVariable(name = "id") Long idPessoa) {
        this.pessoaService.deletePessoa(idPessoa);
    }

    @GetMapping(path = "/{id}/ativar")
    public void ativarPessoa(UsuarioProeidi user, @PathVariable(name = "id") Long idPessoa) {
        this.pessoaService.reativar(idPessoa);
    }

}
