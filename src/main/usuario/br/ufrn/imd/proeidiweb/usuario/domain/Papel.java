package br.ufrn.imd.proeidiweb.usuario.domain;

import br.ufrn.imd.proeidiweb.usuario.domain.enums.TipoPapel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "papel")
@Getter
@Setter
@NoArgsConstructor
public class Papel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "papel")
    @Enumerated(EnumType.STRING)
    private TipoPapel nome;

    @Column(name = "data_criado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado;

    @Column(name = "ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaAtualizacao;

    @Column(name = "ativo")
    private Boolean ativo;

    @JoinColumn(name = "id_pessoa_cadastro")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pessoa pessoaCadastro;

    @JoinColumn(name = "id_pessoa_ultima_atualizacao")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pessoa pessoaUltimaAtualizacao;

    @PrePersist
    @PreUpdate
    private void prePersist() {
        if(this.id == null || this.id.equals(0L)){
            this.dataCriado = new Date();
        }
        this.ultimaAtualizacao = new Date();
    }
}
