package br.ufrn.imd.proeidiweb.usuario.repository.dao;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.usuario.dto.PessoaListItemDTO;
import br.ufrn.imd.proeidiweb.usuario.mapper.database.PessoaListItemDTORowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PessoaDao {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public PageResponse<PessoaListItemDTO> findAll(QueryFilter filtro, boolean somenteMonitores) {
        // Recuperando os dados
        Integer skipRows = (filtro.getPage() - 1) * filtro.getPageSize();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("search", "%" + filtro.getSearch() + "%");
        namedParameters.addValue("pageSize", filtro.getPageSize());
        namedParameters.addValue("skipRows", skipRows);

        String select = "SELECT p.id, p.nome, p.cpf, p.email, p.data_nascimento, p.data_criado, ps.id as id_pessoa_cadastro, ps.nome as nome_pessoa_cadastro, p.ativo as ativo, " +
                "(select string_agg(telefone, ',') from telefone t where t.id_pessoa = p.id group by t.id_pessoa) as telefones ";
        String from = "FROM pessoa p ";
        String join = "LEFT JOIN pessoa ps ON p.id_pessoa_cadastro = ps.id ";
        String where = "WHERE 1 = 1 ";
        if (filtro.getSearch() != null) {
            where += "AND (p.nome LIKE :search OR p.cpf LIKE :search) ";
        }

        if (somenteMonitores) {
            where += "AND (p.id IN (SELECT pp.id_pessoa FROM pessoa_papel pp LEFT JOIN papel pa ON pp.id_papel = pa.id WHERE pa.papel = 'MONITOR')) ";
        } else {
            where += "AND (p.id IN (SELECT pp.id_pessoa FROM pessoa_papel pp LEFT JOIN papel pa ON pp.id_papel = pa.id WHERE pa.papel = 'ALUNO')) ";
        }

        String order = "ORDER BY p.nome limit :pageSize offset :skipRows ";
        List<PessoaListItemDTO> content = jdbcTemplate.query(select + from + join +
                where + order, namedParameters, new PessoaListItemDTORowMapper());

        // Recuperar a quantidade
        String sbCounter = "SELECT count(p.id) as count " + from + where;
        long counter = 0L;
        SqlRowSet countResult = jdbcTemplate.queryForRowSet(sbCounter, namedParameters);

        while (countResult.next()) {
            counter = countResult.getLong("count");
        }

        long totalPages = ((counter - 1) / filtro.getPageSize()) + 1;

        PageResponse<PessoaListItemDTO> response = new PageResponse<>();
        response.setCurrentPage(filtro.getPage());
        response.setPageSize(filtro.getPageSize());
        response.setContent(content);
        response.setTotalElements(counter);
        response.setTotalPages(totalPages);

        return response;
    }
}
