package br.ufrn.imd.proeidiweb.usuario.domain.enums;

public enum TipoPapel {
    COORDENADOR("Coordenador"),
    MONITOR("Monitor"),
    ALUNO("Aluno");

    private String descricao;

    TipoPapel(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
