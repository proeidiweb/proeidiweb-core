package br.ufrn.imd.proeidiweb.usuario.dto;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PapelDTO extends TipoDTO {
    public PapelDTO(Papel papel) {
        this.id = papel.getId();
        this.nome = papel.getNome().getDescricao();
    }
}
