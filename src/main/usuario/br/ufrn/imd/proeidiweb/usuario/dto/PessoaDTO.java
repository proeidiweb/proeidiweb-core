package br.ufrn.imd.proeidiweb.usuario.dto;

import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PessoaDTO extends PessoaListItemDTO {
    private String matricula;
    private String rg;

    private String usuario;
    private Date dataUltimaAtualizacao;

    List<PapelDTO> papeis;

    public PessoaDTO(Pessoa pessoa) {
        super(pessoa);
        this.matricula = pessoa.getMatricula();
        this.rg = pessoa.getRg();
        this.usuario = pessoa.getUsuario();
        this.dataUltimaAtualizacao = pessoa.getDataUltimaAtualizacao();

        papeis = new ArrayList<>();

        for (Papel papel : pessoa.getPapeis()) {
            papeis.add(new PapelDTO(papel));
        }
    }
}
