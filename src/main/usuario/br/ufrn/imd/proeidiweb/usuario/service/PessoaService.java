package br.ufrn.imd.proeidiweb.usuario.service;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.auth.validators.SenhaValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.domain.enums.TipoPapel;
import br.ufrn.imd.proeidiweb.usuario.dto.PessoaListItemDTO;
import br.ufrn.imd.proeidiweb.usuario.repository.PapelRepository;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import br.ufrn.imd.proeidiweb.usuario.repository.dao.PessoaDao;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PessoaService {
    private final PessoaRepository pessoaRepository;
    private final PessoaDao pessoaDao;
    private final PapelRepository papelRepository;

    @Transactional
    public Long saveMonitor(UsuarioProeidi user, Pessoa pessoa) {
        Pessoa pessoaBd = new Pessoa();
        Pessoa usuarioBd = this.pessoaRepository.findById(user.getId()).orElseThrow(ResourceNotFoundException::new);
        // Cadastrando Monitor
        if(pessoa.getId() == null || pessoa.getId().equals(0L)) {
            pessoaBd.setSenha(SenhaValidatorUtils.encryptPassword(pessoa.getSenha()));
            List<Papel> papeis = new ArrayList<>();
            papeis.add(this.papelRepository.findPapelByNome(TipoPapel.MONITOR));
            pessoaBd.setMatricula(generateMatricula());
            pessoaBd.setPapeis(papeis);
            pessoaBd.setPessoaCadastro(usuarioBd);
        } else {
            pessoaBd = this.pessoaRepository.findById(pessoa.getId()).orElseThrow(ResourceNotFoundException::new);
        }

        BeanUtils.copyProperties(pessoa, pessoaBd, Pessoa.ignoreProperties);

        Papel papel = this.papelRepository.findPapelByNome(TipoPapel.COORDENADOR);
        if (pessoa.isPermissaoCoordenador()) {
            if (!pessoaBd.getPapeis().contains(papel)) {
                pessoaBd.getPapeis().add(papel);
            }
        } else {
            pessoaBd.getPapeis().remove(papel);
        }

        pessoaBd.setPessoaUltimaAtualizacao(usuarioBd);

        return this.pessoaRepository.saveAndFlush(pessoaBd).getId();
    }

    private String generateMatricula() {
        LocalDate ld = LocalDate.now();
        return "" + ld.getYear() + ld.getMonthValue() + (new Date()).getTime();
    }

    @Transactional
    public void deletePessoa(Long idPessoa) {
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        pessoa.setAtivo(false);
        this.pessoaRepository.saveAndFlush(pessoa);
    }

    @Transactional
    public void reativar(Long idPessoa) {
        Pessoa pessoa = this.findOne(idPessoa);
        pessoa.setAtivo(true);
        this.pessoaRepository.saveAndFlush(pessoa);
    }

    public Pessoa findOne(Long idPessoa) {
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        Hibernate.initialize(pessoa.getPapeis());
        Papel coord = this.papelRepository.findPapelByNome(TipoPapel.COORDENADOR);
        pessoa.setPermissaoCoordenador(pessoa.getPapeis().contains(coord));

        return pessoa;
    }

    public List<Pessoa> findAll() {
        return this.pessoaRepository.findAll();
    }

    public PageResponse<PessoaListItemDTO> findAllByAtivo(QueryFilter filtro, boolean somenteMonitores) {
        return this.pessoaDao.findAll(filtro, somenteMonitores);
    }


}
