package br.ufrn.imd.proeidiweb.usuario.rest;

import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.repository.PapelRepository;
import br.ufrn.imd.proeidiweb.usuario.service.PapelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/papel")
@RequiredArgsConstructor
public class PapelRestController {
    private final PapelRepository papelRepository;
    private final PapelService papelService;

    @GetMapping
    public List<Papel> findAllPapeis(UsuarioProeidi user){
        return this.papelRepository.findAll();
    }

    @PostMapping
    public void savePapel(UsuarioProeidi user, @RequestBody Papel papel) {
        this.papelService.savePapel(papel);
    }

    @DeleteMapping
    public void deletePapel(UsuarioProeidi user, @RequestParam(name = "id_papel") Long idPapel) {
        this.papelService.deletePapel(idPapel);
    }
}
