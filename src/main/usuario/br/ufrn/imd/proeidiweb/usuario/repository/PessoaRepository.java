package br.ufrn.imd.proeidiweb.usuario.repository;

import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
    @Query("SELECT p FROM Pessoa p WHERE p.ativo = :ativo")
    List<Pessoa> findAllByAtivo(@Param("ativo") Boolean ativo);

    @Query("SELECT p FROM Pessoa p WHERE (p.usuario = :campo OR p.email = :campo OR p.cpf = :campo ) AND p.ativo = TRUE")
    Optional<Pessoa> findByUsuarioOrEmailOrCpfAndAtivo(@Param("campo") String campo);
}
