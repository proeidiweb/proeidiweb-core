package br.ufrn.imd.proeidiweb.usuario.dto;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PessoaListItemDTO {
    private Long id;

    private String nome;
    private String cpf;
    private String email;

    private Date dataNascimento;
    private Date dataCriado;
    private TipoDTO pessoaCadastro;
    private boolean ativo;
    private List<String> telefones;

    public PessoaListItemDTO(Pessoa pessoa) {
        this.id = pessoa.getId();
        this.nome = pessoa.getNome();
        this.cpf = pessoa.getCpf();
        this.email = pessoa.getEmail();
        this.dataNascimento = pessoa.getDataNascimento();
        this.dataCriado = pessoa.getDataCriado();
        if (pessoa.getPessoaCadastro() != null) {
            this.pessoaCadastro = new TipoDTO(pessoa.getPessoaCadastro().getId(), pessoa.getPessoaCadastro().getNome());
        }
        this.telefones = pessoa.getTelefones();
        this.ativo = pessoa.getAtivo();
    }
}
