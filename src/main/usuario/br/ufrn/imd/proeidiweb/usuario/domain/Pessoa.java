package br.ufrn.imd.proeidiweb.usuario.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "pessoa")
@Getter
@Setter
@NoArgsConstructor
public class Pessoa {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "matricula")
    private String matricula;
    @Column(name = "nome")
    private String nome;
    @Column(name = "cpf")
    private String cpf;
    @Column(name = "rg")
    private String rg;
    @Column(name = "data_nascimento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascimento;
    @Column(name = "email")
    private String email;

    @Column(name = "usuario")
    private String usuario;
    @Column(name = "senha")
    private String senha;

    @Column(name = "data_criado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado;
    @Column(name = "ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaAtualizacao;
    @Column(name = "ativo")
    private Boolean ativo;
    @Column(name = "user_token")
    private String token;
    @Column(name = "data_token")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataToken;

    @ManyToOne
    @JoinColumn(name = "id_pessoa_cadastro")
    private Pessoa pessoaCadastro;

    @ManyToOne
    @JoinColumn(name = "id_pessoa_ultima_atualizacao")
    private Pessoa pessoaUltimaAtualizacao;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "pessoa_papel",
            joinColumns = @JoinColumn(name = "id_pessoa"),
            inverseJoinColumns = @JoinColumn(name = "id_papel"))
    @JsonIgnoreProperties({"pessoaUltimaAtualizacao", "pessoaCadastro"})
    @Fetch(FetchMode.SUBSELECT)
    private List<Papel> papeis;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "telefone", joinColumns = @JoinColumn(name = "id_pessoa"))
    @Column(name = "telefone")
    private List<String> telefones;

    @Transient
    private boolean permissaoCoordenador = false;

    @Transient
    public static final String[] ignoreProperties = {"matricula", "senha", "papeis", "pessoaCadastro", "dataCriado", "dataUltimaAtualizacao", "ativo", "pessoaCadastro", "pessoaUltimaAtualizacao", "token", "dataToken", "ignoreProperties"};

    @PreUpdate
    @PrePersist
    private void prePersist(){
        if(this.id == null || this.id.equals(0L)){
            this.dataCriado = new Date();
            this.ativo = true;
        }

        this.dataUltimaAtualizacao = new Date();
    }
}
