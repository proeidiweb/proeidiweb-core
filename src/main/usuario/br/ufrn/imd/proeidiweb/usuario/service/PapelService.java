package br.ufrn.imd.proeidiweb.usuario.service;

import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.repository.PapelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PapelService {
    private final PapelRepository papelRepository;

    @Transactional
    public void savePapel(Papel papel) {
        papel.setAtivo(true);
        this.papelRepository.saveAndFlush(papel);
    }

    public Papel findPapel(Long idPapel) {
        Papel papel = this.papelRepository.findById(idPapel).orElseThrow(ResourceNotFoundException::new);
        return papel;
    }

    @Transactional
    public void deletePapel(Long idPapel) {
        Papel papel = this.papelRepository.findById(idPapel).orElseThrow(ResourceNotFoundException::new);
        // TODO Recuperar o usuário e verificar se ele é Coordenador
        this.papelRepository.delete(papel);
    }
}
