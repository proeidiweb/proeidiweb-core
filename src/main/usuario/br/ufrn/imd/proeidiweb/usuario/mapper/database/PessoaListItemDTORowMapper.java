package br.ufrn.imd.proeidiweb.usuario.mapper.database;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.usuario.dto.PessoaListItemDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class PessoaListItemDTORowMapper implements RowMapper<PessoaListItemDTO> {
    @Override
    public PessoaListItemDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        PessoaListItemDTO pessoa = new PessoaListItemDTO();
        pessoa.setId(rs.getLong("id"));
        pessoa.setNome(rs.getString("nome"));
        pessoa.setCpf(rs.getString("cpf"));
        pessoa.setDataCriado(rs.getTimestamp("data_criado"));
        pessoa.setDataNascimento(rs.getTimestamp("data_nascimento"));
        pessoa.setEmail(rs.getString("email"));
        pessoa.setAtivo(rs.getBoolean("ativo"));
        TipoDTO pessoaCadastro = new TipoDTO(rs.getLong("id_pessoa_cadastro"), rs.getString("nome_pessoa_cadastro"));
        pessoa.setPessoaCadastro(pessoaCadastro);

        String tels = rs.getString("telefones");
        if (tels != null) {
            if (tels.contains(",")) {
                pessoa.setTelefones(Arrays.asList(tels.split(",")));
            } else {
                pessoa.setTelefones(Arrays.asList(tels));
            }
        }

        return pessoa;
    }
}
