package br.ufrn.imd.proeidiweb.forum.mapper.database;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.forum.dto.RespostaDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RespostaDTORowMapper implements RowMapper<RespostaDTO> {
    @Override
    public RespostaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        RespostaDTO resposta = new RespostaDTO();
        resposta.setId(rs.getLong("id"));
        resposta.setNome(rs.getString("nome"));
        resposta.setDataCriado(rs.getTimestamp("data_criado"));
        resposta.setDataUltimaAtualizacao(rs.getTimestamp("data_ultima_atualizacao"));

        TipoDTO pessoa = new TipoDTO(rs.getLong("id_pessoa"), rs.getString("nome_pessoa"));
        resposta.setPessoaCadastro(pessoa);

        TipoDTO duvida = new TipoDTO(rs.getLong("id_duvida"), rs.getString("nome_duvida"));
        resposta.setDuvida(duvida);

        return resposta;
    }
}
