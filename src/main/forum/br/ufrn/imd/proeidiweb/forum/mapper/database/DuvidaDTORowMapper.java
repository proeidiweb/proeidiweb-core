package br.ufrn.imd.proeidiweb.forum.mapper.database;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.forum.dto.DuvidaDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DuvidaDTORowMapper implements RowMapper<DuvidaDTO> {
    @Override
    public DuvidaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        DuvidaDTO duvida = new DuvidaDTO();
        duvida.setId(rs.getLong("id"));
        duvida.setNome(rs.getString("nome"));
        duvida.setDataCriado(rs.getTimestamp("data_criado"));
        duvida.setDataUltimaAtualizacao(rs.getTimestamp("data_ultima_atualizacao"));

        TipoDTO pessoa = new TipoDTO(rs.getLong("id_pessoa"), rs.getString("nome_pessoa"));
        duvida.setPessoaCadastro(pessoa);

        TipoDTO forum = new TipoDTO(rs.getLong("id_forum"), rs.getString("nome_forum"));
        duvida.setForum(forum);

        return duvida;
    }
}
