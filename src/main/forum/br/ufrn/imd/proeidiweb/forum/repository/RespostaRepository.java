package br.ufrn.imd.proeidiweb.forum.repository;

import br.ufrn.imd.proeidiweb.forum.domain.Resposta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespostaRepository extends JpaRepository<Resposta, Long> {}
