package br.ufrn.imd.proeidiweb.forum.service;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.forum.domain.Duvida;
import br.ufrn.imd.proeidiweb.forum.domain.Resposta;
import br.ufrn.imd.proeidiweb.forum.dto.RespostaDTO;
import br.ufrn.imd.proeidiweb.forum.mapper.response.RespostaDTOMapper;
import br.ufrn.imd.proeidiweb.forum.repository.DuvidaRepository;
import br.ufrn.imd.proeidiweb.forum.repository.RespostaRepository;
import br.ufrn.imd.proeidiweb.forum.repository.dao.RespostaDao;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.domain.enums.TipoPapel;
import br.ufrn.imd.proeidiweb.usuario.dto.PapelDTO;
import br.ufrn.imd.proeidiweb.usuario.repository.PapelRepository;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class RespostaService {
    private final RespostaRepository respostaRepository;
    private final RespostaDao respostaDao;
    private final PessoaRepository pessoaRepository;
    private final PapelRepository papelRepository;
    private final DuvidaRepository duvidaRepository;
    private final RespostaDTOMapper mapper;

    @Transactional
    public Long save(Resposta resposta, Long idPessoa) throws BusinessRuleException {
        Resposta respostaBd = new Resposta();
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        if (resposta.getId() != null) {
            respostaBd = this.respostaRepository.findById(resposta.getId()).orElseThrow(ResourceNotFoundException::new);
        } else {
            respostaBd.setPessoaCadastro(pessoa);
            Duvida duvida = this.duvidaRepository.findById(resposta.getDuvida().getId()).orElseThrow(ResourceNotFoundException::new);
            respostaBd.setDataCriado(new Date());
            respostaBd.setAtivo(true);
            respostaBd.setDuvida(duvida);
        }
        respostaBd.setNome(resposta.getNome());
        respostaBd.setPessoaUltimaAtualizacao(pessoa);
        respostaBd.setDataUltimaAtualizacao(new Date());

        if (!respostaBd.getPessoaCadastro().equals(pessoa)) {
            throw new BusinessRuleException("Você não cadastrou essa resposta");
        }

        return this.respostaRepository.save(respostaBd).getId();
    }

    @Transactional
    public void delete(Long idPessoa, Long idForum, PapelDTO papelSelecionado) {
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        Hibernate.initialize(pessoa.getPapeis());
        Resposta duvida = this.respostaRepository.findById(idForum).orElseThrow(ResourceNotFoundException::new);

        // Se o usuário logado for quem cadastrou
        if (duvida.getPessoaCadastro().equals(pessoa)) {
            this.respostaRepository.delete(duvida);
        }

        // Ou se o usuário logado for um COORDENADOR
        Papel papel = this.papelRepository.findById(papelSelecionado.getId()).orElseThrow(ResourceNotFoundException::new);
        if (papel.getNome().equals(TipoPapel.COORDENADOR)) {
            this.respostaRepository.delete(duvida);
        }
    }

    public PageResponse<RespostaDTO> findAllByForum(Long idDuvida, QueryFilter filtro) {
        return this.respostaDao.findAllByDuvida(idDuvida, filtro);
    }

    public RespostaDTO findOne(Long idResposta) {
        return this.mapper.map(this.respostaRepository.findById(idResposta).orElseThrow(ResourceNotFoundException::new));
    }
}
