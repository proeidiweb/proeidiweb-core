package br.ufrn.imd.proeidiweb.forum.rest;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.forum.domain.Duvida;
import br.ufrn.imd.proeidiweb.forum.dto.DuvidaDTO;
import br.ufrn.imd.proeidiweb.forum.service.DuvidaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/duvida")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class DuvidaRestController {
    private final DuvidaService duvidaService;

    @GetMapping
    public PageResponse<DuvidaDTO> findAllByForum(UsuarioProeidi user, QueryFilter filtro, @RequestParam("id_forum") Long idForum) {
        return this.duvidaService.findAllByForum(idForum, filtro);
    }

    @GetMapping(path = "/{id}")
    public DuvidaDTO findOne(UsuarioProeidi user, @PathVariable("id") Long idDuvida) {
        return this.duvidaService.findOne(idDuvida);
    }

    @PostMapping
    public Long save(UsuarioProeidi user, @RequestBody Optional<Duvida> optDuvida) throws BusinessRuleException {
        if (optDuvida.isEmpty()) {
            throw new BusinessRuleException("Dúvida não pode ser vazia");
        }

        Duvida duvida = optDuvida.get();
        return this.duvidaService.save(duvida, user.getId());
    }

    @DeleteMapping(path = "/{id}")
    public void delete(UsuarioProeidi user, @PathVariable("id") Long idDuvida) {
        this.duvidaService.delete(user.getId(), idDuvida, user.getPapelSelecionado());
    }
}
