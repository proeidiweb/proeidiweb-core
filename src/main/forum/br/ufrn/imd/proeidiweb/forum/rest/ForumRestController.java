package br.ufrn.imd.proeidiweb.forum.rest;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.forum.domain.Forum;
import br.ufrn.imd.proeidiweb.forum.service.ForumService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/forum")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class ForumRestController {
    private final ForumService forumService;

    @GetMapping
    public List<TipoDTO> findForuns(UsuarioProeidi usuario) {
        return this.forumService.findAllByAtivo(true);
    }

    @GetMapping(path = "/{id}")
    public TipoDTO findForum(UsuarioProeidi usuario, @PathVariable("id") Long idForum) {
        return this.forumService.findOne(idForum);
    }

    @PostMapping
    public Long save(UsuarioProeidi usuario, @RequestBody Optional<Forum> optForum) throws BusinessRuleException {
        if (optForum.isEmpty()) {
            throw new BusinessRuleException("Fórum não pode ser vazio");
        }
        Forum forum = optForum.get();

        return this.forumService.save(forum, usuario.getId());
    }

    @DeleteMapping(path = "/{id}")
    public void delete(UsuarioProeidi usuario, @PathVariable("id") Long idForum) {
        this.forumService.delete(usuario.getId(), idForum, usuario.getPapelSelecionado());
    }
}
