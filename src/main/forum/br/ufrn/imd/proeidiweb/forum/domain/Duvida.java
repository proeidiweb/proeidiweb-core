package br.ufrn.imd.proeidiweb.forum.domain;

import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "duvida")
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Duvida {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "duvida")
    private String nome;

    @Column(name = "data_criado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado;

    @Column(name = "ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaAtualizacao;

    @Column(name = "ativo")
    private boolean ativo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_cadastro")
    private Pessoa pessoaCadastro;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_ultima_atualizacao")
    @JsonIgnoreProperties({"pessoaUltimaAtualizacao", "pessoaCadastro"})
    private Pessoa pessoaUltimaAtualizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_forum")
    @JsonIgnoreProperties({"duvidas"})
    private Forum forum;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "duvida", cascade = CascadeType.REMOVE)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Resposta> respostas;
}
