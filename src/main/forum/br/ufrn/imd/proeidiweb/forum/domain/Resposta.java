package br.ufrn.imd.proeidiweb.forum.domain;

import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "resposta")
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Resposta {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "resposta")
    private String nome;

    @Column(name = "data_criado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriado;

    @Column(name = "ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaAtualizacao;

    @Column(name = "ativo")
    private boolean ativo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_cadastro")
    private Pessoa pessoaCadastro;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pessoa_ultima_atualizacao")
    @JsonIgnoreProperties({"pessoaUltimaAtualizacao", "pessoaCadastro"})
    private Pessoa pessoaUltimaAtualizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_duvida")
    @JsonIgnoreProperties({"respostas"})
    private Duvida duvida;
}
