package br.ufrn.imd.proeidiweb.forum.mapper.response;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.forum.domain.Forum;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ForumTipoDTOMapper {
    public TipoDTO map(Forum forum) {
        return new TipoDTO(forum.getId(), forum.getNome());
    }

    public List<TipoDTO> map(List<Forum> foruns) {
        List<TipoDTO> forunsDTO = new ArrayList<>();
        if (foruns != null && !foruns.isEmpty()) {
            foruns.forEach(it -> forunsDTO.add(this.map(it)));
        }
        return forunsDTO;
    }
}
