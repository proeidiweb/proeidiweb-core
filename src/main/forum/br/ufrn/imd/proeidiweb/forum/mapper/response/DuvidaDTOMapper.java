package br.ufrn.imd.proeidiweb.forum.mapper.response;

import br.ufrn.imd.proeidiweb.forum.domain.Duvida;
import br.ufrn.imd.proeidiweb.forum.dto.DuvidaDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DuvidaDTOMapper {
    public DuvidaDTO map(Duvida duvida) {
        return new DuvidaDTO(duvida);
    }

    public List<DuvidaDTO> map(List<Duvida> duvidas) {
        List<DuvidaDTO> dtos = new ArrayList<>();
        if (duvidas != null && !duvidas.isEmpty()) {
            duvidas.forEach(it -> {
                dtos.add(this.map(it));
            });
        }
        return dtos;
    }
}
