package br.ufrn.imd.proeidiweb.forum.dto;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.forum.domain.Resposta;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RespostaDTO extends TipoDTO {
    private Date dataCriado;
    private Date dataUltimaAtualizacao;
    private TipoDTO pessoaCadastro;
    private TipoDTO duvida;

    public RespostaDTO(Resposta resposta) {
        super(resposta.getId(), resposta.getNome());
        this.dataCriado = resposta.getDataCriado();
        this.dataCriado = resposta.getDataUltimaAtualizacao();
        this.pessoaCadastro = new TipoDTO(resposta.getPessoaCadastro().getId(), resposta.getPessoaCadastro().getNome());
        this.duvida = new TipoDTO(resposta.getDuvida().getId(), resposta.getDuvida().getNome());
    }
}
