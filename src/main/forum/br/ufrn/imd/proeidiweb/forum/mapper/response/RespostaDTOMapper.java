package br.ufrn.imd.proeidiweb.forum.mapper.response;

import br.ufrn.imd.proeidiweb.forum.domain.Resposta;
import br.ufrn.imd.proeidiweb.forum.dto.RespostaDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RespostaDTOMapper {
    public RespostaDTO map(Resposta resposta) {
        return new RespostaDTO(resposta);
    }

    public List<RespostaDTO> map(List<Resposta> respostas) {
        List<RespostaDTO> dtos = new ArrayList<>();
        if (respostas != null && !respostas.isEmpty()) {
            respostas.forEach(it -> {
                dtos.add(this.map(it));
            });
        }
        return dtos;
    }
}
