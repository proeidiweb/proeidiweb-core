package br.ufrn.imd.proeidiweb.forum.rest;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.forum.domain.Resposta;
import br.ufrn.imd.proeidiweb.forum.dto.RespostaDTO;
import br.ufrn.imd.proeidiweb.forum.service.RespostaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/resposta")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class RespostaRestController {
    private final RespostaService respostaService;

    @GetMapping
    public PageResponse<RespostaDTO> findAllByDuvida(UsuarioProeidi usuario, QueryFilter filtro, @RequestParam("id_duvida") Long idDuvida) {
        return this.respostaService.findAllByForum(idDuvida, filtro);
    }

    @GetMapping(path = "/{id}")
    public RespostaDTO findOne(UsuarioProeidi usuario, @PathVariable("id") Long idResposta) {
        return this.respostaService.findOne(idResposta);
    }

    @PostMapping
    public Long save(UsuarioProeidi user, @RequestBody Optional<Resposta> optResposta) throws BusinessRuleException {
        if (optResposta.isEmpty()) {
            throw new BusinessRuleException("Resposta não pode ser vazia");
        }

        Resposta resposta = optResposta.get();
        return this.respostaService.save(resposta, user.getId());
    }

    @DeleteMapping(path = "/{id}")
    public void delete(UsuarioProeidi user, @PathVariable("id") Long idResposta) {
        this.respostaService.delete(user.getId(), idResposta, user.getPapelSelecionado());
    }
}
