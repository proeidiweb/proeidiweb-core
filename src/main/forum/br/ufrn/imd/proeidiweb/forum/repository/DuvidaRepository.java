package br.ufrn.imd.proeidiweb.forum.repository;

import br.ufrn.imd.proeidiweb.forum.domain.Duvida;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DuvidaRepository extends JpaRepository<Duvida, Long> {
    List<Duvida> findAllByAtivo(boolean ativo);
}
