package br.ufrn.imd.proeidiweb.forum.service;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.forum.domain.Duvida;
import br.ufrn.imd.proeidiweb.forum.domain.Forum;
import br.ufrn.imd.proeidiweb.forum.dto.DuvidaDTO;
import br.ufrn.imd.proeidiweb.forum.mapper.response.DuvidaDTOMapper;
import br.ufrn.imd.proeidiweb.forum.repository.DuvidaRepository;
import br.ufrn.imd.proeidiweb.forum.repository.ForumRepository;
import br.ufrn.imd.proeidiweb.forum.repository.dao.DuvidaDao;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.domain.enums.TipoPapel;
import br.ufrn.imd.proeidiweb.usuario.dto.PapelDTO;
import br.ufrn.imd.proeidiweb.usuario.repository.PapelRepository;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class DuvidaService {
    private final DuvidaRepository duvidaRepository;
    private final DuvidaDao duvidaDao;
    private final PessoaRepository pessoaRepository;
    private final PapelRepository papelRepository;
    private final ForumRepository forumRepository;
    private final DuvidaDTOMapper mapper;

    @Transactional
    public Long save(Duvida duvida, Long idPessoa) throws BusinessRuleException {
        Duvida duvidaBd = new Duvida();
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        if (duvida.getId() != null) {
            duvidaBd = this.duvidaRepository.findById(duvida.getId()).orElseThrow(ResourceNotFoundException::new);
        } else {
            duvidaBd.setPessoaCadastro(pessoa);
            Forum forum = this.forumRepository.findById(duvida.getForum().getId()).orElseThrow(ResourceNotFoundException::new);
            duvidaBd.setDataCriado(new Date());
            duvidaBd.setAtivo(true);
            duvidaBd.setForum(forum);
        }
        duvidaBd.setNome(duvida.getNome());
        duvidaBd.setPessoaUltimaAtualizacao(pessoa);
        duvidaBd.setDataUltimaAtualizacao(new Date());

        if (!duvidaBd.getPessoaCadastro().equals(pessoa)) {
            throw new BusinessRuleException("Você não cadastrou essa dúvida");
        }

        return this.duvidaRepository.save(duvidaBd).getId();
    }

    @Transactional
    public void delete(Long idPessoa, Long idForum, PapelDTO papelSelecionado) {
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        Hibernate.initialize(pessoa.getPapeis());
        Duvida duvida = this.duvidaRepository.findById(idForum).orElseThrow(ResourceNotFoundException::new);

        // Se o usuário logado for quem cadastrou
        if (duvida.getPessoaCadastro().equals(pessoa)) {
            this.duvidaRepository.delete(duvida);
        }

        // Ou se o usuário logado for um COORDENADOR
        Papel papel = this.papelRepository.findById(papelSelecionado.getId()).orElseThrow(ResourceNotFoundException::new);
        if (papel.getNome().equals(TipoPapel.COORDENADOR)) {
            this.duvidaRepository.delete(duvida);
        }
    }

    public PageResponse<DuvidaDTO> findAllByForum(Long idForum, QueryFilter filtro) {
        return this.duvidaDao.findAllByForum(idForum, filtro);
    }

    public DuvidaDTO findOne(Long idDuvida) {
        return this.mapper.map(this.duvidaRepository.findById(idDuvida).orElseThrow(ResourceNotFoundException::new));
    }
}
