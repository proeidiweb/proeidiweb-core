package br.ufrn.imd.proeidiweb.forum.service;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.forum.domain.Forum;
import br.ufrn.imd.proeidiweb.forum.mapper.response.ForumTipoDTOMapper;
import br.ufrn.imd.proeidiweb.forum.repository.ForumRepository;
import br.ufrn.imd.proeidiweb.usuario.domain.Papel;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.domain.enums.TipoPapel;
import br.ufrn.imd.proeidiweb.usuario.dto.PapelDTO;
import br.ufrn.imd.proeidiweb.usuario.repository.PapelRepository;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ForumService {
    private final ForumRepository forumRepository;
    private final PessoaRepository pessoaRepository;
    private final PapelRepository papelRepository;
    private final ForumTipoDTOMapper mapper;

    @Transactional
    public Long save(Forum forum, Long idPessoa) {
        Forum forumBd = new Forum();
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        if (forum.getId() != null) {
            forumBd = this.forumRepository.findById(forum.getId()).orElseThrow(ResourceNotFoundException::new);
        } else {
            forumBd.setPessoaCadastro(pessoa);
        }
        forumBd.setNome(forum.getNome());
        forumBd.setPessoaUltimaAtualizacao(pessoa);

        return this.forumRepository.save(forumBd).getId();
    }

    @Transactional
    public void delete(Long idPessoa, Long idForum, PapelDTO papelSelecionado) {
        Pessoa pessoa = this.pessoaRepository.findById(idPessoa).orElseThrow(ResourceNotFoundException::new);
        Hibernate.initialize(pessoa.getPapeis());
        Forum forum = this.forumRepository.findById(idForum).orElseThrow(ResourceNotFoundException::new);

        // Se o usuário logado for quem cadastrou
        if (forum.getPessoaCadastro().equals(pessoa)) {
            this.forumRepository.delete(forum);
        }

        // Ou se o usuário logado for um COORDENADOR
        Papel papel = this.papelRepository.findById(papelSelecionado.getId()).orElseThrow(ResourceNotFoundException::new);
        if (papel.getNome().equals(TipoPapel.COORDENADOR)) {
            this.forumRepository.delete(forum);
        }
    }

    public List<TipoDTO> findAll() {
        return this.mapper.map(this.forumRepository.findAll());
    }

    public List<TipoDTO> findAllByAtivo(boolean ativo) {
        return this.mapper.map(this.forumRepository.findAllByAtivo(ativo));
    }

    public TipoDTO findOne(Long idForum) {
        return this.mapper.map(this.forumRepository.findById(idForum).orElseThrow(ResourceNotFoundException::new));
    }
}
