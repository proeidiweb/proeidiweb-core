package br.ufrn.imd.proeidiweb.forum.repository;

import br.ufrn.imd.proeidiweb.forum.domain.Forum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ForumRepository extends JpaRepository<Forum, Long> {
    List<Forum> findAllByAtivo(boolean ativo);
}
