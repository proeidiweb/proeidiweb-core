package br.ufrn.imd.proeidiweb.forum.repository.dao;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.forum.dto.DuvidaDTO;
import br.ufrn.imd.proeidiweb.forum.mapper.database.DuvidaDTORowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DuvidaDao {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public PageResponse<DuvidaDTO> findAllByForum(Long idForum, QueryFilter filtro) {
        // Recuperando os dados

        Integer skipRows = (filtro.getPage() - 1) * filtro.getPageSize();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("forum", idForum);
        namedParameters.addValue("pageSize", filtro.getPageSize());
        namedParameters.addValue("skipRows", skipRows);

        String select = "SELECT d.id as id, d.duvida as nome, d.data_criado as data_criado, d.ultima_atualizacao as data_ultima_atualizacao, p.id as id_pessoa, p.nome as nome_pessoa, f.id as id_forum, f.titulo as nome_forum " +
                "FROM duvida d " +
                "JOIN pessoa p ON p.id = d.id_pessoa_cadastro " +
                "JOIN forum f ON d.id_forum = f.id " +
                "WHERE d.ativo = true AND d.id_forum = :forum " +
                "ORDER BY d.data_criado DESC " +
                "limit :pageSize offset :skipRows";
        List<DuvidaDTO> content = jdbcTemplate.query(select, namedParameters, new DuvidaDTORowMapper());

        // Recuperar as quantidades
        String sbCounter = "SELECT count(d.id) as count FROM duvida d WHERE d.ativo = true AND d.id_forum = :forum";
        long counter = 0L;
        SqlRowSet countResult = jdbcTemplate.queryForRowSet(sbCounter, namedParameters);
        while (countResult.next()) {
            counter = countResult.getLong("count");
        }

        long totalPages = ((counter - 1) / filtro.getPageSize()) + 1;

        PageResponse<DuvidaDTO> response = new PageResponse<>();
        response.setCurrentPage(filtro.getPage());
        response.setPageSize(filtro.getPageSize());
        response.setContent(content);
        response.setTotalElements(counter);
        response.setTotalPages(totalPages);

        return response;
    }
}
