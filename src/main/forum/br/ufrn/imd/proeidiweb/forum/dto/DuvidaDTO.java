package br.ufrn.imd.proeidiweb.forum.dto;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.forum.domain.Duvida;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DuvidaDTO extends TipoDTO {
    private Date dataCriado;
    private Date dataUltimaAtualizacao;
    private TipoDTO pessoaCadastro;
    private TipoDTO forum;

    public DuvidaDTO(Duvida duvida) {
        super(duvida.getId(), duvida.getNome());
        this.dataCriado = duvida.getDataCriado();
        this.dataUltimaAtualizacao = duvida.getDataUltimaAtualizacao();
        this.pessoaCadastro = new TipoDTO(duvida.getPessoaCadastro().getId(), duvida.getPessoaCadastro().getNome());
        this.forum = new TipoDTO(duvida.getForum().getId(), duvida.getForum().getNome());
    }
}
