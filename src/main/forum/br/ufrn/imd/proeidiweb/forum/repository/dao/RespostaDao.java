package br.ufrn.imd.proeidiweb.forum.repository.dao;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.forum.dto.RespostaDTO;
import br.ufrn.imd.proeidiweb.forum.mapper.database.RespostaDTORowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RespostaDao  {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public PageResponse<RespostaDTO> findAllByDuvida(Long idDuvida, QueryFilter filtro) {
        // Recuperando os dados
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT r.id as id, r.resposta as nome, r.data_criado as data_criado, r.ultima_atualizacao as data_ultima_atualizacao, p.id as id_pessoa, p.nome as nome_pessoa, d.id as id_duvida, d.duvida as nome_duvida ");
        sb.append("FROM resposta r ");
        sb.append("JOIN pessoa p ON p.id = r.id_pessoa_cadastro ");
        sb.append("JOIN duvida d ON r.id_duvida = d.id ");
        sb.append("WHERE r.ativo = true AND r.id_duvida = :duvida ");
        sb.append("ORDER BY r.data_criado ASC ");
        sb.append("limit :pageSize offset :skipRows");

        Integer skipRows = (filtro.getPage() - 1) * filtro.getPageSize();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("duvida", idDuvida);
        namedParameters.addValue("pageSize", filtro.getPageSize());
        namedParameters.addValue("skipRows", skipRows);

        List<RespostaDTO> content = jdbcTemplate.query(sb.toString(), namedParameters, new RespostaDTORowMapper());

        // Recuperar as quantidades
        String sbCounter = "SELECT count(r.id) as count FROM resposta r WHERE r.ativo = true AND r.id_duvida = :duvida";
        Long counter = 0L;
        SqlRowSet countResult = jdbcTemplate.queryForRowSet(sbCounter, namedParameters);
        while (countResult.next()) {
            counter = countResult.getLong("count");
        }

        long totalPages = ((counter - 1) / filtro.getPageSize()) + 1;

        PageResponse<RespostaDTO> response = new PageResponse<>();
        response.setCurrentPage(filtro.getPage());
        response.setPageSize(filtro.getPageSize());
        response.setContent(content);
        response.setTotalElements(counter);
        response.setTotalPages(totalPages);

        return response;
    }
}
