package br.ufrn.imd.proeidiweb.matricula.repository;

import br.ufrn.imd.proeidiweb.matricula.domain.PeriodoMatricula;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PeriodoMatriculaRepository extends JpaRepository<PeriodoMatricula, Long> {

    @Query(value = "SELECT pm.* FROM periodo_matricula pm ORDER BY pm.data_inicio DESC limit :limit offset :offset", nativeQuery = true)
    List<PeriodoMatricula> findAllPageable(@Param("limit") int limit, @Param("offset") int offset);

    @Query("SELECT pm FROM PeriodoMatricula pm WHERE pm.dataInicio <= :data AND pm.dataFim >= :data AND pm.ativo = true")
    List<PeriodoMatricula> findAllByDataInicioAndDataFimBetweenAndAtivo(@Param("data") Date data);
}
