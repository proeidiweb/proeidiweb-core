package br.ufrn.imd.proeidiweb.matricula.service;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.comum.service.ServiceComumComplete;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.comum.validators.PermissaoValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.matricula.domain.Disciplina;
import br.ufrn.imd.proeidiweb.matricula.domain.Sala;
import br.ufrn.imd.proeidiweb.matricula.dto.DisciplinaDTO;
import br.ufrn.imd.proeidiweb.matricula.mapper.response.DisciplinaDTOMapper;
import br.ufrn.imd.proeidiweb.matricula.repository.DisciplinaRepository;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DisciplinaService implements ServiceComumComplete<Disciplina, DisciplinaDTO> {
    private final DisciplinaRepository disciplinaRepository;
    private final PessoaRepository pessoaRepository;

    @Override
    @Transactional
    public Long save(UsuarioProeidi user, Disciplina objeto) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        Disciplina disciplina = new Disciplina();
        Pessoa pessoa = this.pessoaRepository.findById(user.getId()).orElseThrow(ResourceNotFoundException::new);
        if (objeto.getId() != null) {
            disciplina = this.findOne(objeto.getId());
        } else {
            disciplina.setDataCriado(new Date());
            disciplina.setPessoaCadastro(pessoa);
        }

        BeanUtils.copyProperties(objeto, disciplina, Sala.ignoreProperties);

        disciplina.setDataUltimaAtualizacao(new Date());
        disciplina.setPessoaUltimaAtualizacao(pessoa);
        disciplina.setAtivo(true);

        this.validar(disciplina);

        return this.disciplinaRepository.save(disciplina).getId();
    }

    @Override
    public void validar(Disciplina objeto) throws BusinessRuleException {
        if (objeto.getNome() == null || objeto.getDescricao() == null || objeto.getCargaHoraria() == null) {
            throw new BusinessRuleException("Nome, Descrição ou Carga Horária são obrigatórios");
        }
    }

    @Override
    public Disciplina findOne(Long id) {
        return this.disciplinaRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public List<DisciplinaDTO> findAllByAtivo() {
        return this.disciplinaRepository.findAllByAtivoIsTrue().stream().map(DisciplinaDTOMapper::map).collect(Collectors.toList());
    }

    @Override
    public PageResponse<Disciplina> findAllPageable(QueryFilter filtro) {
        int skipRows = (filtro.getPage() - 1) * filtro.getPageSize();
        PageResponse<Disciplina> response = new PageResponse<>();
        List<Disciplina> disciplinas;
        long totalElements;

        if (filtro.getSearch() != null) {
            String search = "%" + filtro.getSearch() + "%";
            disciplinas = this.disciplinaRepository.findAllPageable(search, filtro.getPageSize(), skipRows);
            totalElements = this.disciplinaRepository.count(search);
        } else {
            disciplinas = this.disciplinaRepository.findAllPageable(filtro.getPageSize(), skipRows);
            totalElements = this.disciplinaRepository.count();
        }

        long totalPages = ((totalElements - 1) / filtro.getPageSize()) + 1;

        response.setPageSize(filtro.getPageSize());
        response.setCurrentPage(filtro.getPage());
        response.setTotalPages(totalPages);
        response.setTotalElements(totalElements);
        response.setContent(disciplinas);

        return response;
    }

    @Override
    @Transactional
    public void desativar(UsuarioProeidi user, Long id) {
        Disciplina disciplina = this.findOne(id);
        disciplina.setAtivo(false);
        this.disciplinaRepository.save(disciplina);
    }
}
