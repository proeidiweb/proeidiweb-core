package br.ufrn.imd.proeidiweb.matricula.mapper.response;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.matricula.domain.Sala;

public class SalaTipoDTOMapper {
    public static TipoDTO map(Sala sala) {
        return new TipoDTO(sala.getId(), sala.getNome());
    }
}
