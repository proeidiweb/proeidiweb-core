package br.ufrn.imd.proeidiweb.matricula.repository;

import br.ufrn.imd.proeidiweb.matricula.domain.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DisciplinaRepository extends JpaRepository<Disciplina, Long> {
    List<Disciplina> findAllByAtivoIsTrue();

    @Query(value = "SELECT d.* FROM disciplina d ORDER BY d.titulo limit :limit offset :offset", nativeQuery = true)
    List<Disciplina> findAllPageable(@Param("limit") int pageSize, @Param("offset") int skipRows);

    @Query(value = "SELECT d.* FROM disciplina d WHERE d.titulo LIKE :search OR d.descricao LIKE :search ORDER BY d.titulo limit :limit offset :offset", nativeQuery = true)
    List<Disciplina> findAllPageable(@Param("search") String search, @Param("limit") int pageSize, @Param("offset") int skipRows);

    @Query(value = "SELECT count(d.id) FROM disciplina d WHERE d.titulo LIKE :search OR d.descricao LIKE :search", nativeQuery = true)
    Long count(@Param("search") String search);
}
