package br.ufrn.imd.proeidiweb.matricula.repository;

import br.ufrn.imd.proeidiweb.matricula.domain.Sala;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SalaRepository extends JpaRepository<Sala, Long> {

    @Query("SELECT s FROM Sala s WHERE s.ativo = TRUE ORDER BY s.nome")
    List<Sala> findAllByAtivoIsTrue();

    @Query(value = "SELECT s.* FROM sala s ORDER BY s.nome limit :limit offset :offset", nativeQuery = true)
    List<Sala> findAllPageable(@Param("limit") int limit, @Param("offset") int offset);

    @Query(value = "SELECT s.* FROM sala s WHERE s.nome LIKE :search OR s.descricao LIKE :search ORDER BY s.nome limit :limit offset :offset", nativeQuery = true)
    List<Sala> findAllPageable(@Param("search") String search, @Param("limit") int limit, @Param("offset") int offset);

    @Query(value = "SELECT count(s.id) FROM sala s WHERE s.nome LIKE :search OR s.descricao LIKE :search", nativeQuery = true)
    Long count(@Param("search") String search);
}
