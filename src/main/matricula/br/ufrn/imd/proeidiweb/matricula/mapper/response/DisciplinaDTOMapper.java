package br.ufrn.imd.proeidiweb.matricula.mapper.response;

import br.ufrn.imd.proeidiweb.matricula.domain.Disciplina;
import br.ufrn.imd.proeidiweb.matricula.dto.DisciplinaDTO;

public class DisciplinaDTOMapper {
    public static DisciplinaDTO map(Disciplina disciplina) {
        return new DisciplinaDTO(disciplina.getId(), disciplina.getNome(), disciplina.getDescricao());
    }
}
