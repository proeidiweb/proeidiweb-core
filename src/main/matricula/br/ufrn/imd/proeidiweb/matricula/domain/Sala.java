package br.ufrn.imd.proeidiweb.matricula.domain;

import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "sala")
public class Sala {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "ativo")
    private boolean ativo = true;

    @Column(name = "data_criado")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date dataCriado;

    @Column(name = "ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date dataUltimaAtualizacao;

    @ManyToOne
    @JoinColumn(name = "id_pessoa_cadastro")
    @CreatedBy
    private Pessoa pessoaCadastro;

    @ManyToOne
    @JoinColumn(name = "id_pessoa_ultima_atualizacao")
    @LastModifiedBy
    private Pessoa pessoaUltimaAtualizacao;

    @Transient
    public static final String[] ignoreProperties = {"dataUltimaAtualizacao", "dataCriado", "pessoaCadastro", "pessoaUltimaAtualizacao", "id", "ativo"};
}
