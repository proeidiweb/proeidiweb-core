package br.ufrn.imd.proeidiweb.matricula.rest;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.comum.validators.PermissaoValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.matricula.domain.Disciplina;
import br.ufrn.imd.proeidiweb.matricula.dto.DisciplinaDTO;
import br.ufrn.imd.proeidiweb.matricula.service.DisciplinaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/disciplina")
@RequiredArgsConstructor
public class DisciplinaRestController {
    private final DisciplinaService disciplinaService;

    @GetMapping(path = "")
    public List<DisciplinaDTO> findAll(UsuarioProeidi user) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        return this.disciplinaService.findAllByAtivo();
    }

    @GetMapping(path = "/paginado")
    public PageResponse<Disciplina> findAll(UsuarioProeidi user, QueryFilter filtro) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        return this.disciplinaService.findAllPageable(filtro);
    }

    @PostMapping(path = "")
    public Long save(UsuarioProeidi user, @RequestBody Optional<Disciplina> disciplina) throws BusinessRuleException {
        if (disciplina.isEmpty()) {
            throw new BusinessRuleException("Disciplina não pode ser nula");
        }

        return this.disciplinaService.save(user, disciplina.get());
    }

    @GetMapping(path = "/{id}")
    public Disciplina findOne(UsuarioProeidi user, @PathVariable("id") Long idSala) {
        return this.disciplinaService.findOne(idSala);
    }

    @DeleteMapping(path = "/{id}")
    public void desativar(UsuarioProeidi user, @PathVariable("id") Long idSala) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        this.disciplinaService.desativar(user, idSala);
    }
}
