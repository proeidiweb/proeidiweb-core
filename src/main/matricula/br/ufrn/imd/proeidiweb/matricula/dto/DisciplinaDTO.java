package br.ufrn.imd.proeidiweb.matricula.dto;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DisciplinaDTO extends TipoDTO {
    private String descricao;

    public DisciplinaDTO(Long id, String nome, String descricao) {
        super(id, nome);
        this.descricao = descricao;
    }
}
