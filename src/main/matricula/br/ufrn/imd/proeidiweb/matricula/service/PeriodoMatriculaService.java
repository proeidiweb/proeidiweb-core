package br.ufrn.imd.proeidiweb.matricula.service;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.comum.service.ServiceComum;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.matricula.domain.PeriodoMatricula;
import br.ufrn.imd.proeidiweb.matricula.repository.PeriodoMatriculaRepository;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PeriodoMatriculaService implements ServiceComum<PeriodoMatricula> {
    private final PeriodoMatriculaRepository periodoMatriculaRepository;
    private final PessoaRepository pessoaRepository;

    @Transactional
    @Override
    public Long save(UsuarioProeidi user, PeriodoMatricula periodo) throws BusinessRuleException {
        Pessoa pessoa = this.pessoaRepository.findById(user.getId()).orElseThrow(ResourceNotFoundException::new);
        PeriodoMatricula periodoBd = new PeriodoMatricula();
        if (periodo.getId() != null) {
            periodoBd = this.findOne(periodo.getId());
        } else {
            periodoBd.setDataCadastro(new Date());
            periodoBd.setPessoaCadastro(pessoa);
        }

        BeanUtils.copyProperties(periodo, periodoBd, PeriodoMatricula.ignoreProperties);

        periodoBd.setDataUltimaAtualizacao(new Date());
        periodoBd.setPessoaUltimaAtualizacao(pessoa);
        periodoBd.setAtivo(true);

        this.validar(periodoBd);

        return this.periodoMatriculaRepository.save(periodoBd).getId();
    }

    @Override
    public void validar(PeriodoMatricula periodoMatricula) throws BusinessRuleException {
        if(periodoMatricula.getDataInicio() == null || periodoMatricula.getDataFim() == null) {
            throw new BusinessRuleException("Data de início ou fim não podem ser vazios");
        }

        if (periodoMatricula.getDataFim().compareTo(periodoMatricula.getDataInicio()) < 0) {
            throw new BusinessRuleException("Data de fim não pode ser anterior ao início");
        }

        List<PeriodoMatricula> periodoDatasInicio = this.periodoMatriculaRepository.findAllByDataInicioAndDataFimBetweenAndAtivo(periodoMatricula.getDataInicio());
        if (!periodoDatasInicio.isEmpty()) {
            if (!periodoDatasInicio.contains(periodoMatricula)) {
                throw new BusinessRuleException("Essa data de início está em um intervalo de um outro periodo");
            }
        }

        List<PeriodoMatricula> periodoDatasFim = this.periodoMatriculaRepository.findAllByDataInicioAndDataFimBetweenAndAtivo(periodoMatricula.getDataFim());
        if(!periodoDatasFim.isEmpty()) {
            if (!periodoDatasFim.contains(periodoMatricula)) {
                throw new BusinessRuleException("Essa data de término está em um intervalo de um outro periodo");
            }
        }
    }

    @Override
    public PageResponse<PeriodoMatricula> findAllPageable(QueryFilter filtro) {
        Integer skipRows = (filtro.getPage() - 1) * filtro.getPageSize();
        PageResponse<PeriodoMatricula> response = new PageResponse<>();

        List<PeriodoMatricula> periodos = this.periodoMatriculaRepository.findAllPageable(filtro.getPageSize(), skipRows);
        long totalElements = this.periodoMatriculaRepository.count();
        long totalPages = ((totalElements - 1) / filtro.getPageSize()) + 1;

        response.setPageSize(filtro.getPageSize());
        response.setCurrentPage(filtro.getPage());
        response.setTotalPages(totalPages);
        response.setTotalElements(totalElements);
        response.setContent(periodos);

        return response;
    }

    @Override
    public PeriodoMatricula findOne(Long idPeriodoMatricula) {
        return this.periodoMatriculaRepository.findById(idPeriodoMatricula).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional
    public void delete(Long idPeriodoMatricula) {
        PeriodoMatricula periodoMatricula = this.findOne(idPeriodoMatricula);
        this.periodoMatriculaRepository.delete(periodoMatricula);
    }

    @Transactional
    @Override
    public void desativar(UsuarioProeidi user, Long idPeriodoMatricula) {
        Pessoa pessoa = this.pessoaRepository.findById(user.getId()).orElseThrow(ResourceNotFoundException::new);
        PeriodoMatricula periodoMatricula = this.findOne(idPeriodoMatricula);
        periodoMatricula.setPessoaUltimaAtualizacao(pessoa);
        periodoMatricula.setDataUltimaAtualizacao(new Date());
        periodoMatricula.setAtivo(false);
        this.periodoMatriculaRepository.save(periodoMatricula);
    }
}
