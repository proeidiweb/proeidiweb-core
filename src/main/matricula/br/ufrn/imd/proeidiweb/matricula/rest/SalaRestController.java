package br.ufrn.imd.proeidiweb.matricula.rest;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.comum.validators.PermissaoValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.matricula.domain.Sala;
import br.ufrn.imd.proeidiweb.matricula.service.SalaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/sala")
@RequiredArgsConstructor
public class SalaRestController {
    private final SalaService salaService;

    @GetMapping(path = "")
    public List<TipoDTO> findAll(UsuarioProeidi user) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        return this.salaService.findAllByAtivo();
    }

    @GetMapping(path = "/paginado")
    public PageResponse<Sala> findAll(UsuarioProeidi user, QueryFilter filtro) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        return this.salaService.findAllPageable(filtro);
    }

    @PostMapping(path = "")
    public Long save(UsuarioProeidi user, @RequestBody Optional<Sala> sala) throws BusinessRuleException {
        if (sala.isEmpty()) {
            throw new BusinessRuleException("Sala não pode ser nula");
        }

        return this.salaService.save(user, sala.get());
    }

    @GetMapping(path = "/{id}")
    public Sala findOne(UsuarioProeidi user, @PathVariable("id") Long idSala) {
        return this.salaService.findOne(idSala);
    }

    @DeleteMapping(path = "/{id}")
    public void desativar(UsuarioProeidi user, @PathVariable("id") Long idSala) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        this.salaService.desativar(user, idSala);
    }
}
