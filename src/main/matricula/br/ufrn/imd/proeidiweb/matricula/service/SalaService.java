package br.ufrn.imd.proeidiweb.matricula.service;

import br.ufrn.imd.proeidiweb.comum.dto.TipoDTO;
import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.comum.service.ServiceComumComplete;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.comum.validators.PermissaoValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.core.exceptions.ResourceNotFoundException;
import br.ufrn.imd.proeidiweb.matricula.domain.Sala;
import br.ufrn.imd.proeidiweb.matricula.mapper.response.SalaTipoDTOMapper;
import br.ufrn.imd.proeidiweb.matricula.repository.SalaRepository;
import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import br.ufrn.imd.proeidiweb.usuario.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SalaService implements ServiceComumComplete<Sala, TipoDTO> {
    private final SalaRepository salaRepository;
    private final PessoaRepository pessoaRepository;

    @Transactional
    @Override
    public Long save(UsuarioProeidi user, Sala sala) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        Sala salaBd = new Sala();
        Pessoa pessoa = this.pessoaRepository.findById(user.getId()).orElseThrow(ResourceNotFoundException::new);
        if (sala.getId() != null) {
            salaBd = this.findOne(sala.getId());
        } else {
            salaBd.setDataCriado(new Date());
            salaBd.setPessoaCadastro(pessoa);
        }

        BeanUtils.copyProperties(sala, salaBd, Sala.ignoreProperties);

        salaBd.setDataUltimaAtualizacao(new Date());
        salaBd.setPessoaUltimaAtualizacao(pessoa);
        salaBd.setAtivo(true);

        this.validar(salaBd);

        return this.salaRepository.save(salaBd).getId();
    }

    @Override
    public void validar(Sala sala) throws BusinessRuleException {
        if (sala.getNome() == null || sala.getDescricao() == null) {
            throw new BusinessRuleException("Campos nome e descrição são obrigatórios");
        }
    }

    @Override
    public PageResponse<Sala> findAllPageable(QueryFilter filtro) {
        int skipRows = (filtro.getPage() - 1) * filtro.getPageSize();
        PageResponse<Sala> response = new PageResponse<>();
        List<Sala> salas;
        long totalElements;

        if (filtro.getSearch() != null) {
            String search = "%" + filtro.getSearch() + "%";
            salas = this.salaRepository.findAllPageable(search, filtro.getPageSize(), skipRows);
            totalElements = this.salaRepository.count(search);
        } else {
            salas = this.salaRepository.findAllPageable(filtro.getPageSize(), skipRows);
            totalElements = this.salaRepository.count();
        }

        long totalPages = ((totalElements - 1) / filtro.getPageSize()) + 1;

        response.setPageSize(filtro.getPageSize());
        response.setCurrentPage(filtro.getPage());
        response.setTotalPages(totalPages);
        response.setTotalElements(totalElements);
        response.setContent(salas);

        return response;
    }

    @Override
    public List<TipoDTO> findAllByAtivo() {
        return this.salaRepository.findAllByAtivoIsTrue().stream().map(SalaTipoDTOMapper::map).collect(Collectors.toList());
    }

    @Override
    public Sala findOne(Long idSala) {
        return this.salaRepository.findById(idSala).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional
    @Override
    public void desativar(UsuarioProeidi user, Long idSala) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        Sala sala = this.findOne(idSala);
        sala.setAtivo(false);
        this.salaRepository.save(sala);
    }
}
