package br.ufrn.imd.proeidiweb.matricula.rest;

import br.ufrn.imd.proeidiweb.comum.query.QueryFilter;
import br.ufrn.imd.proeidiweb.comum.response.PageResponse;
import br.ufrn.imd.proeidiweb.core.auth.users.UsuarioProeidi;
import br.ufrn.imd.proeidiweb.comum.validators.PermissaoValidatorUtils;
import br.ufrn.imd.proeidiweb.core.exceptions.BusinessRuleException;
import br.ufrn.imd.proeidiweb.matricula.domain.PeriodoMatricula;
import br.ufrn.imd.proeidiweb.matricula.service.PeriodoMatriculaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/periodo-matricula")
@RequiredArgsConstructor
public class PeriodoMatriculaRestController {
    private final PeriodoMatriculaService periodoMatriculaService;

    @GetMapping(path = "")
    public PageResponse<PeriodoMatricula> findAll(UsuarioProeidi user, QueryFilter filtro) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        return this.periodoMatriculaService.findAllPageable(filtro);
    }

    @PostMapping(path = "")
    public Long save(UsuarioProeidi user, @RequestBody Optional<PeriodoMatricula> periodo) throws BusinessRuleException {
        if (periodo.isEmpty()) {
            throw new BusinessRuleException("Período não pode ser núlo");
        }

        return this.periodoMatriculaService.save(user, periodo.get());
    }

    @GetMapping(path = "/{id}")
    public PeriodoMatricula findOne(UsuarioProeidi user, @PathVariable("id") Long idPeriodoMatricula) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        return this.periodoMatriculaService.findOne(idPeriodoMatricula);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(UsuarioProeidi user, @PathVariable("id") Long idPeriodoMatricula) throws BusinessRuleException {
        PermissaoValidatorUtils.permissaoCoordenador(user);
        this.periodoMatriculaService.desativar(user, idPeriodoMatricula);
    }
}
