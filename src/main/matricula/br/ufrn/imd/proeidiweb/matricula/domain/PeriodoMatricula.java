package br.ufrn.imd.proeidiweb.matricula.domain;

import br.ufrn.imd.proeidiweb.usuario.domain.Pessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "periodo_matricula")
public class PeriodoMatricula {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "ativo")
    private boolean ativo = false;

    @Column(name = "data_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;

    @Column(name = "data_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFim;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date dataCadastro;

    @Column(name = "ultima_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date dataUltimaAtualizacao;

    @ManyToOne
    @JoinColumn(name = "id_pessoa_cadastro")
    @CreatedBy
    private Pessoa pessoaCadastro;

    @ManyToOne
    @JoinColumn(name = "id_pessoa_ultima_atualizacao")
    @LastModifiedBy
    private Pessoa pessoaUltimaAtualizacao;

    @Transient
    public static final String[] ignoreProperties = {"dataUltimaAtualizacao", "dataCadastro", "pessoaCadastro", "pessoaUltimaAtualizacao", "id", "ativo"};
}
